package by.davidowski.commons.interfaces;

import by.davidowski.commons.messaging.Message;

public interface CallbackHandler {
    void handleCallback(Message message);
}
