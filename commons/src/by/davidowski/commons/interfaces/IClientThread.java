package by.davidowski.commons.interfaces;

import by.davidowski.commons.messaging.Message;

import java.io.IOException;

public interface IClientThread {
    void sendMessage(Message message) throws IOException;
    void registerCallbackHandler(CallbackHandler callbackHandler);
    void finish();

}
