package by.davidowski.commons.interfaces;

import by.davidowski.commons.messaging.Message;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public interface MessageDispatcher <T extends Thread & IClientThread> {

    void registerClientThread(T client);
    void closeDispatcher();

    default void dispatchMessage(Message message) throws NotImplementedException{
        throw new NotImplementedException();
    }

    default void sispatchMessage(Message message,T client){

    }
}
