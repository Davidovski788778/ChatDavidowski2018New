package by.davidowski.client.core;

import by.davidowski.commons.threading.ClientThread;

public class Core {//ещё синглтоны
    private int portNumber;
    private String hostAddess;
    private String userName;
    private ClientThread clientThread;

    private static volatile Core instance = null;

    private Core() {
    }

    public static Core getInstance() {
        if(instance == null){
            synchronized (Core.class){
                if(instance == null){
                    instance = new Core();
                }
            }
        }
        return instance;
    }



    public int getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    public String getHostAddess() {
        return hostAddess;
    }

    public void setHostAddess(String hostAddess) {
        this.hostAddess = hostAddess;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public ClientThread getClientThread() {
        return this.clientThread;
    }

    public void setClientThread(ClientThread clientThread) {
        this.clientThread = clientThread;
    }

}
