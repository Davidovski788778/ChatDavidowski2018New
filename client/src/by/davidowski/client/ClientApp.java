package by.davidowski.client;

import by.davidowski.client.core.Core;
import by.davidowski.client.screen.ScreenManager;
import by.davidowski.client.screen.Screens;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class ClientApp extends Application{

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage window) throws Exception {
        ScreenManager.getInstance().registerWindow(window);
        ScreenManager.getInstance().switchScreen(Screens.LOGIN_SCREEN);
        window.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, event -> Core.getInstance().getClientThread().finish());
        window.show();
    }
}
