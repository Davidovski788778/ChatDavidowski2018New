package by.davidowski.client.logic;

import by.davidowski.commons.messaging.Message;

public interface IControllerBridge {
    void handleLogicEvent(Message message);
}
