package by.davidowski.client.logic;

import by.davidowski.commons.interfaces.CallbackHandler;
import by.davidowski.commons.interfaces.IClientThread;
import by.davidowski.commons.interfaces.MessageDispatcher;
import by.davidowski.commons.messaging.Message;

public abstract class Logic<T extends Thread & IClientThread> implements MessageDispatcher<T>, CallbackHandler{

    protected IControllerBridge bridge;

    public Logic(IControllerBridge bridge){
        this.bridge = bridge;
    }

    public IControllerBridge getBridge() {
        return bridge;
    }

    public void setBridge(IControllerBridge bridge) {
        this.bridge = bridge;
    }

    public abstract void sendMessage(Message message);
}
