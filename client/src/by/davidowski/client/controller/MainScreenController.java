package by.davidowski.client.controller;

import by.davidowski.client.core.Core;
import by.davidowski.client.logic.IControllerBridge;
import by.davidowski.client.logic.Logic;
import by.davidowski.client.logic.TCPLogic;
import by.davidowski.client.screen.ScreenManager;
import by.davidowski.commons.messaging.Message;
import by.davidowski.commons.messaging.PublicMessage;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import java.net.URL;
import java.util.ResourceBundle;

public class MainScreenController extends BaseController implements IControllerBridge{
    @FXML
    private TextArea chatLog;
    @FXML
    private TextArea message;

    private Logic logic;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        logic = new TCPLogic(this);
        logic.registerClientThread(Core.getInstance().getClientThread());
        ScreenManager.getInstance().getWindow().setTitle("Chat - " + Core.getInstance().getUserName());
    }
    @FXML
    private void keyPressed(KeyEvent e) {
        if(e.isControlDown()&& e.getCode() == KeyCode.ENTER){
            send();
        }
    }

    @FXML
    private void sendButtonClicked(MouseEvent e) {
        send();
    }

    @Override
    public void handleLogicEvent(Message message) {
        chatLog.appendText(message.toString());
    }

    private void send (){//создание сообщения
        PublicMessage message = new PublicMessage(this.message.getText(),Core.getInstance().getUserName());
        logic.sendMessage(message);
        this.message.clear();
    }
}
