package by.davidowski.client.controller;

import by.davidowski.client.screen.ScreenManager;
import by.davidowski.client.screen.Screens;
import javafx.fxml.Initializable;

import java.io.IOException;

public abstract class BaseController implements Initializable {
    protected void navigate(Screens screen) throws IOException {
        ScreenManager.getInstance().switchScreen(screen);
    }
}
